/*
ds18b20 lib 0x01

copyright (c) Davide Gironi, 2012

Released under GPLv3.
Please refer to LICENSE file for licensing information.
*/

#include <avr/io.h>
#include <util/delay.h>

#include "ds18b20.h"

static const uint8_t dscrc_table[256] = {
    0, 94, 188, 226, 97, 63, 221, 131, 194, 156,
    126, 32, 163, 253, 31, 65, 157, 195, 33, 127, 252, 162, 64, 30, 95, 1,
    227, 189, 62, 96, 130, 220, 35, 125, 159, 193, 66, 28, 254, 160, 225,
    191, 93, 3, 128, 222, 60, 98, 190, 224, 2, 92, 223, 129, 99, 61, 124,
    34, 192, 158, 29, 67, 161, 255, 70, 24, 250, 164, 39, 121, 155, 197,
    132, 218, 56, 102, 229, 187, 89, 7, 219, 133, 103, 57, 186, 228, 6, 88,
    25, 71, 165, 251, 120, 38, 196, 154, 101, 59, 217, 135, 4, 90, 184, 230,
    167, 249, 27, 69, 198, 152, 122, 36, 248, 166, 68, 26, 153, 199, 37,
    123, 58, 100, 134, 216, 91, 5, 231, 185, 140, 210, 48, 110, 237, 179,
    81, 15, 78, 16, 242, 172, 47, 113, 147, 205, 17, 79, 173, 243, 112, 46,
    204, 146, 211, 141, 111, 49, 178, 236, 14, 80, 175, 241, 19, 77, 206,
    144, 114, 44, 109, 51, 209, 143, 12, 82, 176, 238, 50, 108, 142, 208,
    83, 13, 239, 177, 240, 174, 76, 18, 145, 207, 45, 115, 202, 148, 118,
    40, 171, 245, 23, 73, 8, 86, 180, 234, 105, 55, 213, 139, 87, 9, 235,
    181, 54, 104, 138, 212, 149, 203, 41, 119, 244, 170, 72, 22, 233, 183,
    85, 11, 136, 214, 52, 106, 43, 117, 151, 201, 74, 20, 246, 168, 116, 42,
200, 150, 21, 75, 169, 247, 182, 232, 10, 84, 215, 137, 107, 53 };

/*
 * ds18b20 init
 */
uint8_t ds18b20_reset(void) {
    uint8_t i;

    //low for 480us
    DS18B20_PORT &= ~ (1<<DS18B20_DQ); //low
    DS18B20_DDR |= (1<<DS18B20_DQ); //output
    _delay_us(480);

    //release line and wait for 60uS
    DS18B20_DDR &= ~(1<<DS18B20_DQ); //input
    _delay_us(60);

    //get value and wait 420us
    i = (DS18B20_PIN & (1<<DS18B20_DQ));
    _delay_us(420);

    //return the read value, 0=ok, 1=error
    return i;
}

/*
 * write one bit
 */
void ds18b20_writebit(uint8_t bit){
    //low for 1uS
    DS18B20_PORT &= ~ (1<<DS18B20_DQ); //low
    DS18B20_DDR |= (1<<DS18B20_DQ); //output
    _delay_us(1);

    //if we want to write 1, release the line (if not will keep low)
    if(bit)
        DS18B20_DDR &= ~(1<<DS18B20_DQ); //input

    //wait 60uS and release the line
    _delay_us(60);
    DS18B20_DDR &= ~(1<<DS18B20_DQ); //input
}

/*
 * read one bit
 */
uint8_t ds18b20_readbit(void){
    uint8_t bit=0;

    //low for 1uS
    DS18B20_PORT &= ~ (1<<DS18B20_DQ); //low
    DS18B20_DDR |= (1<<DS18B20_DQ); //output
    _delay_us(1);

    //release line and wait for 14uS
    DS18B20_DDR &= ~(1<<DS18B20_DQ); //input
    _delay_us(14);

    //read the value
    if(DS18B20_PIN & (1<<DS18B20_DQ))
        bit=1;

    //wait 45uS and return read value
    _delay_us(45);
    return bit;
}

/*
 * write one byte
 */
void ds18b20_writebyte(uint8_t byte){
    uint8_t i=8;
    while(i--){
        ds18b20_writebit(byte&1);
        byte >>= 1;
    }
}

uint8_t ds18b20_crc(uint8_t crc, uint8_t x) {
    return dscrc_table[crc ^ x];
}

/*
 * read one byte
 */
uint8_t ds18b20_readbyte(void){
    uint8_t i=8, n=0;
    //uint8_t crc = 0;

    while(i--){
        n >>= 1;
        n |= (ds18b20_readbit()<<7);
    }
    //crc = ds18b20_crc(crc, n);
    //if (crc > 0) return 0xff;
    
    return n;
}

/*
 * get temperature
 */
double ds18b20_gettemp(void) {
    uint8_t temperature[2];
    int8_t digit;
    uint16_t decimal;
    double retd = 0;

    ds18b20_reset(); //reset
    ds18b20_writebyte(DS18B20_CMD_SKIPROM); //skip ROM
    ds18b20_writebyte(DS18B20_CMD_CONVERTTEMP); //start temperature conversion

    while(!ds18b20_readbit()); //wait until conversion is complete

    ds18b20_reset(); //reset
    ds18b20_writebyte(DS18B20_CMD_SKIPROM); //skip ROM
    ds18b20_writebyte(DS18B20_CMD_RSCRATCHPAD); //read scratchpad

    //read 2 byte from scratchpad
    temperature[0] = ds18b20_readbyte();
    temperature[1] = ds18b20_readbyte();

    if (temperature[0] == 0xff && temperature[1] == 0xff) return 11.11;
    if (temperature[0] == 0x00 && temperature[1] == 0x00) return 12.12;
    ds18b20_reset(); //reset

    //store temperature integer digits
    digit = temperature[0]>>4;
    digit |= (temperature[1]&0x7)<<4;

    //store temperature decimal digits
    decimal = temperature[0]&0xf;
    decimal *= DS18B20_DECIMALSTEPS;

    //compose the double temperature value and return it
    retd = digit + decimal * 0.0001;

    return retd;
}

