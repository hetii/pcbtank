/*
 * Examples:
 *
 * #define LED_DIR BITP(DDRB, PB4); // LED_DIR is "variable" pointing to single bit (PB4) in DDRB
 * #define LED_STATE BITP(PORTB, PB4); // LED_STATE points to single bit (PB4) in PORTB
 * LED_DIR = OUT; // Set pin as output
 * LED_STATE = ON; // Switch LED on
 * 
 * BITS_SET(DDRB, PB3, PB5, PB7); // Set bits PB3, PB5 and PB7 in DDRB, leave other bits untouched.
 * BITS_SET_EX(DDRB, PB7); // Sets only PB7 in DDRB, other bits are zeroed (set exclusive).
 * BITS_CLEAR(PORTB, PB4); // Clear bit PB4 in PORTB, leave other bits untouched.
 */

#include <avr/io.h>

/*
 * Struct to handle bits in registers
 */
typedef struct {
	uint8_t b0:1;
	uint8_t b1:1;
	uint8_t b2:1;
	uint8_t b3:1;
	uint8_t b4:1;
	uint8_t b5:1;
	uint8_t b6:1;
	uint8_t b7:1;
} bitmask_s;

/*
 * Helper macros for bit pointer macro
 */
#define BNAME(x) BSTR(x)
#define BSTR(x) b##x

/*
 * Common defines
 */
#define OUT 1
#define IN 0
#define ON 1
#define OFF 0

/*
 * Concatenate helper
 */
#define CONCAT(a, ...) CONCAT_IMPL(a, __VA_ARGS__)
#define CONCAT_IMPL(x, ...) x ## __VA_ARGS__

#define xCONCAT(...) CONCAT_IMPL( __VA_ARGS__)
#define xCONCAT_IMPL(x, ...) x ## __VA_ARGS__

/*
 * Count number of arguments in __VA_ARGS__ 
 */
#define VA_NUM_ARGS_IMPL(_1, _2, _3, _4, _5, _6, _7, _8, x, ...) x
#define VA_NUM_ARGS(...) VA_NUM_ARGS_IMPL(__VA_ARGS__, 8, 7, 6, 5, 4, 3, 2, 1)

/*
 * Bit-Shift-Or macros for 1-8 parameters 
 */
#define BSO_1(a) (1 << a)
#define BSO_2(a, b) (1 << a) | (1 << b)
#define BSO_3(a, b, c) (1 << a) | (1 << b) | (1 << c)
#define BSO_4(a, b, c, d) (1 << a) | (1 << b) | (1 << c) | (1 << d)
#define BSO_5(a, b, c, d, e) (1 << a) | (1 << b) | (1 << c) | (1 << d) | (1 << e)
#define BSO_6(a, b, c, d, e, f) (1 << a) | (1 << b) | (1 << c) | (1 << d) | (1 << e) | (1 << f)
#define BSO_7(a, b, c, d, e, f, g) (1 << a) | (1 << b) | (1 << c) | (1 << d) | (1 << e) | (1 << f) | (1 << g)
#define BSO_8(a, b, c, d, e, f, g, h) (1 << a) | (1 << b) | (1 << c) | (1 << d) | (1 << e) | (1 << f) | (1 << g) | (1 << h)

/*
 * Bit pointer macro which can be used to make individual
 * bit variables
 */
#define BITP(var, bit) ((volatile bitmask_s*)&var)->BNAME(bit)

/* 
 * Macros to set and clear multiple bits in registers
 */
#define BITS_SET(x, ...) x |= (CONCAT(BSO_, VA_NUM_ARGS(__VA_ARGS__))(__VA_ARGS__))
#define BITS_CLEAR(x, ...) x &= ~(CONCAT(BSO_, VA_NUM_ARGS(__VA_ARGS__))(__VA_ARGS__))
#define BITS_SET_EX(x, ...) x = (CONCAT(BSO_, VA_NUM_ARGS(__VA_ARGS__))(__VA_ARGS__))

#ifdef PA0 
  #define PPA0 BITP(PORTA, PA0);
#endif

#ifdef PA1 
  #define PPA1 BITP(PORTA, PA1);
#endif

#ifdef PA2 
  #define PPA2 BITP(PORTA, PA2);
#endif

#ifdef PA3 
  #define PPA3 BITP(PORTA, PA3);
#endif

#ifdef PA4 
  #define PPA4 BITP(PORTA, PA4);
#endif

#ifdef PA5 
  #define PPA5 BITP(PORTA, PA5);
#endif

#ifdef PA6 
  #define PPA6 BITP(PORTA, PA6);
#endif

#ifdef PA7 
  #define PPA7 BITP(PORTA, PA7);
#endif

#ifdef PB0 
  #define PPB0 BITP(PORTB, PB0);
#endif

#ifdef PB1 
  #define PPB1 BITP(PORTB, PB1);
#endif

#ifdef PB2 
  #define PPB2 BITP(PORTB, PB2);
#endif

#ifdef PB3 
  #define PPB3 BITP(PORTB, PB3);
#endif

#ifdef PB4 
  #define PPB4 BITP(PORTB, PB4);
#endif

#ifdef PB5 
  #define PPB5 BITP(PORTB, PB5);
#endif

#ifdef PB6 
  #define PPB6 BITP(PORTB, PB6);
#endif

#ifdef PB7 
  #define PPB7 BITP(PORTB, PB7);
#endif

#ifdef PC0 
  #define PPC0 BITP(PORTC, PC0);
#endif

#ifdef PC1 
  #define PPC1 BITP(PORTC, PC1);
#endif

#ifdef PC2 
  #define PPC2 BITP(PORTC, PC2);
#endif

#ifdef PC3 
  #define PPC3 BITP(PORTC, PC3);
#endif

#ifdef PC4 
  #define PPC4 BITP(PORTC, PC4);
#endif

#ifdef PC5 
  #define PPC5 BITP(PORTC, PC5);
#endif

#ifdef PC6 
  #define PPC6 BITP(PORTC, PC6);
#endif

#ifdef PC7 
  #define PPC7 BITP(PORTC, PC7);
#endif

#ifdef PD0 
  #define PPD0 BITP(PORTD, PD0);
#endif

#ifdef PD1 
  #define PPD1 BITP(PORTD, PD1);
#endif

#ifdef PD2 
  #define PPD2 BITP(PORTD, PD2);
#endif

#ifdef PD3 
  #define PPD3 BITP(PORTD, PD3);
#endif

#ifdef PD4 
  #define PPD4 BITP(PORTD, PD4);
#endif

#ifdef PD5 
  #define PPD5 BITP(PORTD, PD5);
#endif

#ifdef PD6 
  #define PPD6 BITP(PORTD, PD6);
#endif

#ifdef PD7 
  #define PPD7 BITP(PORTD, PD7);
#endif

#ifdef PE0 
  #define PPE0 BITP(PORTE, PE0);
#endif

#ifdef PE1 
  #define PPE1 BITP(PORTE, PE1);
#endif

#ifdef PE2 
  #define PPE2 BITP(PORTE, PE2);
#endif

#ifdef PE3 
  #define PPE3 BITP(PORTE, PE3);
#endif

#ifdef PE4 
  #define PPE4 BITP(PORTE, PE4);
#endif

#ifdef PE5 
  #define PPE5 BITP(PORTE, PE5);
#endif

#ifdef PE6 
  #define PPE6 BITP(PORTE, PE6);
#endif

#ifdef PE7 
  #define PPE7 BITP(PORTE, PE7);
#endif

#ifdef PF0 
  #define PPF0 BITP(PORTF, PF0);
#endif

#ifdef PF1 
  #define PPF1 BITP(PORTF, PF1);
#endif

#ifdef PF2 
  #define PPF2 BITP(PORTF, PF2);
#endif

#ifdef PF3 
  #define PPF3 BITP(PORTF, PF3);
#endif

#ifdef PF4 
  #define PPF4 BITP(PORTF, PF4);
#endif

#ifdef PF5 
  #define PPF5 BITP(PORTF, PF5);
#endif

#ifdef PF6 
  #define PPF6 BITP(PORTF, PF6);
#endif

#ifdef PF7 
  #define PPF7 BITP(PORTF, PF7);
#endif

#ifdef PG0 
  #define PPG0 BITP(PORTG, PG0);
#endif

#ifdef PG1 
  #define PPG1 BITP(PORTG, PG1);
#endif

#ifdef PG2 
  #define PPG2 BITP(PORTG, PG2);
#endif

#ifdef PG3 
  #define PPG3 BITP(PORTG, PG3);
#endif

#ifdef PG4 
  #define PPG4 BITP(PORTG, PG4);
#endif

#ifdef PG5 
  #define PPG5 BITP(PORTG, PG5);
#endif

#ifdef PG6 
  #define PPG6 BITP(PORTG, PG6);
#endif

#ifdef PG7 
  #define PPG7 BITP(PORTG, PG7);
#endif

#ifdef PH0 
  #define PPH0 BITP(PORTH, PH0);
#endif

#ifdef PH1 
  #define PPH1 BITP(PORTH, PH1);
#endif

#ifdef PH2 
  #define PPH2 BITP(PORTH, PH2);
#endif

#ifdef PH3 
  #define PPH3 BITP(PORTH, PH3);
#endif

#ifdef PH4 
  #define PPH4 BITP(PORTH, PH4);
#endif

#ifdef PH5 
  #define PPH5 BITP(PORTH, PH5);
#endif

#ifdef PH6 
  #define PPH6 BITP(PORTH, PH6);
#endif

#ifdef PH7 
  #define PPH7 BITP(PORTH, PH7);
#endif

#ifdef PJ0 
  #define PPJ0 BITP(PORTJ, PJ0);
#endif

#ifdef PJ1 
  #define PPJ1 BITP(PORTJ, PJ1);
#endif

#ifdef PJ2 
  #define PPJ2 BITP(PORTJ, PJ2);
#endif

#ifdef PJ3 
  #define PPJ3 BITP(PORTJ, PJ3);
#endif

#ifdef PJ4 
  #define PPJ4 BITP(PORTJ, PJ4);
#endif

#ifdef PJ5 
  #define PPJ5 BITP(PORTJ, PJ5);
#endif

#ifdef PJ6 
  #define PPJ6 BITP(PORTJ, PJ6);
#endif

#ifdef PJ7 
  #define PPJ7 BITP(PORTJ, PJ7);
#endif

#define A0 00
#define A1 01
#define A2 02
#define A3 03
#define A4 04
#define A5 05
#define A6 06
#define A7 07
#define B0 10
#define B1 11
#define B2 12
#define B3 13
#define B4 14
#define B5 15
#define B6 16
#define B7 17
#define C0 20
#define C1 21
#define C2 22
#define C3 23
#define C4 24
#define C5 25
#define C6 26
#define C7 27
#define D0 30
#define D1 31
#define D2 32
#define D3 33
#define D4 34
#define D5 35
#define D6 36
#define D7 37
#define E0 40
#define E1 41
#define E2 42
#define E3 43
#define E4 44
#define E5 45
#define E6 46
#define E7 47
#define F0 50
#define F1 51
#define F2 52
#define F3 53
#define F4 54
#define F5 55
#define F6 56
#define F7 57
#define G0 60
#define G1 61
#define G2 62
#define G3 63
#define G4 64
#define G5 65
#define G6 66
#define G7 67

#define GPIO_OUTPUT 1
#define GPIO_OUT_OPEN_DRAIN 2

inline void gpio_high(uint8_t pin){

  switch (pin){
#ifdef PORTA
    case A0...A7:
      PORTA |= (1<<pin%10);
      break;
#endif
    
#ifdef PORTB
    case B0...B7:
      PORTB |= (1<<pin%10);
      break;
#endif
    
#ifdef PORTC
    case C0...C7:
      PORTC |= (1<<pin%10);
      break;
#endif
    
#ifdef PORTD
    case D0...D7:
      PORTD |= (1<<pin%10);
      break;
#endif
    
#ifdef PORTE
    case E0...E7:
      PORTE |= (1<<pin%10);
      break;
#endif
    
#ifdef PORTF
    case F0...F7:
      PORTF |= (1<<pin%10);
      break;
#endif
    
#ifdef PORTG
    case G0...G7:
      PORTG |= (1<<pin%10);
      break;
#endif
    
#ifdef PORTH
    case H0...H7:
      PORTH |= (1<<pin%10);
      break;
#endif
    
#ifdef PORTJ
    case J0...J7:
      PORTJ |= (1<<pin%10);
      break;
#endif

  }

}
/*
 (((P) >= 0 && (P) <= 7)   ? PORTA |= (1<<P%10): \
         (((P) >= 10 && (P) <= 17) ? PORTB |= (1<<P%10): \
         (((P) >= 20 && (P) <= 27) ? PORTC |= (1<<P%10): \
                     (((P) >= 30 && (P) <= 37) ? PORTD |= (1<<P%10): \
                     (((P) >= 40 && (P) <= 47) ? PORTE |= (1<<P%10): \
                     (((P) >= 50 && (P) <= 57) ? PORTF |= (1<<P%10): \
                     (((P) >= 60 && (P) <= 67) ? PORTG |= (1<<P%10))))))))
*/


uint8_t gpio_read(uint8_t pin){return 0;}
void gpio_enable(uint8_t pin, uint8_t mode){}
void gpio_write(uint8_t pin, uint8_t state){}

