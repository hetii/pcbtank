#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "ds18b20.h"

#define HEATER_ON PORTC &= ~(1<<PC5)
#define HEATER_OFF PORTC |= (1<<PC5)

#define POMP_ON PORTD &= ~(1<<PD0)
#define POMP_OFF PORTD |= (1<<PD0)

#define LIGHT_OFF PORTB &= ~(1<<PB5)
#define LIGHT_ON PORTB |= (1<<PB5)

#define LEFT_SW (!(PINB & (1<<PB3)))
#define RIGHT_SW (!(PINB & (1<<PB4)))

#define A PORTC &= ~(1<<PC1)
#define B PORTD &= ~(1<<PD2)
#define C PORTC &= ~(1<<PC3)
#define D PORTB &= ~(1<<PB1)
#define E PORTC &= ~(1<<PC4)
#define F PORTC &= ~(1<<PC2)
#define G PORTD &= ~(1<<PD3)
#define H PORTC &= ~(1<<PC0)
#define S_OFF PORTB |= (1<<PB1);PORTC |= (1<<PC0)|(1<<PC1)|(1<<PC2)|(1<<PC3)|(1<<PC4);PORTD |= (1<<PD2)|(1<<PD3)
#define S_INIT S_OFF; DDRB |= (1<<PB1);DDRC |= (1<<PC0)|(1<<PC1)|(1<<PC2)|(1<<PC3)|(1<<PC4);DDRD |= (1<<PD2)|(1<<PD3)

#define T1 PORTD &= ~(1<<PD6)
#define T2 PORTB &= ~(1<<PB0)
#define T3 PORTD &= ~(1<<PD7)
#define T4 PORTD &= ~(1<<PD5)
#define T_OFF PORTD |= (1<<PD5)|(1<<PD6)|(1<<PD7);PORTB |= (1<<PB0)
#define T_INIT T_OFF; DDRD |= (1<<PD5)|(1<<PD6)|(1<<PD7);DDRB |= (1<<PB0)

volatile uint8_t dot=0;
volatile uint8_t allow_dot=1;
volatile uint8_t heater_state=1;
volatile int8_t use_pomp=0;

char digits[4];
uint8_t desired_temp = 43;

static void heater_on(void){
  HEATER_ON;
  heater_state = 1;
}

static void heater_off(void){
  HEATER_OFF;
  heater_state = 0;
}

static void port_init(void){

  DDRC |= (1<<PC5); // HEATER 
  DDRD |= (1<<PD0); // POMP
  DDRB |= (1<<PB5); // LIGHT

  DDRB &= ~(1<<PB3); // LEFT_SW
  DDRB &= ~(1<<PB4); // RIGHT_SW

  PORTB |= (1<<PB3)|(1<<PB4); // LEFT_SW & RIGHT_SW pullups

  S_INIT;
  T_INIT;
  heater_off();
  POMP_OFF;
}

static void set_digit(char c){
  switch(c){
    case 3: T1; break;
    case 2: T2; break;
    case 1: T3; break;
    case 0: T4; break;
    default: T_OFF; break;
  }
}

static void set_char(char c, uint8_t dot){
  // given a number, set the appropraite segments
  T_OFF;
  S_OFF;
  if (dot) H;
  switch(c){
    case 0: A;B;C;D;E;F; break;
    case 1: B;C; break;
    case 2: A;B;G;E;D; break;
    case 3: A;B;G;C;D; break;
    case 4: F;G;B;C; break;
    case 5: A;F;G;C;D; break;
    case 6: A;F;G;E;C;D; break;
    case 7: A;B;C; break;
    case 8: A;B;C;D;E;F;G; break;
    case 9: A;B;C;D;F;G; break;
    case 31: H; break;
    default: S_OFF; break;
  }
}

uint8_t prepare_digits(double value, char *buf, uint8_t y){

  uint8_t x;
  uint8_t pos=0;
  for (x=1; x<value&&y>0; x*=10) y--;
  pos = y;
  while(y--) value*=10;
  while ((uint16_t)value > 0) {
     buf[++y] = (uint16_t)value % 10;
     value /= 10;
  }
  return pos;
}

static void light_blink(uint8_t amount){
  while(amount--){
      LIGHT_OFF;
      _delay_ms(200);
      LIGHT_ON;
      _delay_ms(200);
  }
  LIGHT_ON;
}

// 8-bit timer ISR just to update display.
ISR(TIMER0_OVF_vect){
  static int8_t dd = 3;
  if (dd < 0){
    dd = 3;
  }
  set_char(digits[dd], (dot == dd && dot > 0&& allow_dot));
  set_digit(dd--);
  TCNT0 = 200;
}

// Timer for 1s period.
ISR(TIMER1_COMPA_vect){
  // When heater work, blink dot sign.
  if (heater_state == 1){
    allow_dot ^= 1;
  }else{
    allow_dot = 1;
  }

  if (use_pomp == desired_temp){
    POMP_ON;
  }else if (use_pomp>=1){
    POMP_ON;
    use_pomp--;
    if (use_pomp == 0) use_pomp = -6;
  } else if(use_pomp<0){
    POMP_OFF;
    use_pomp++;
    if (use_pomp == 0) use_pomp = 3;
  }
}

int main (void){
  
  port_init();

  TIMSK |= (1 << TOIE0)| (1 << OCIE1A);        // Enable overflow interrupt
  TCCR0 |= (1 << CS00) | (1<<CS01);            // F_CPU/64
  TCCR1B|= (1 << CS11) | (1<<CS10)|(1<<WGM12); // F_CPU/64 CTC

  OCR1A = 15625; //1000000(F_CPU)/64(prescaler) = 1s
  TCNT0 = 155;
  TCNT1 = 0;
  sei(); // Enable global interrupts

  uint8_t was_click = 0;
  while (1){

    double temp = ds18b20_gettemp();
 
    if (temp >= desired_temp && heater_state == 1){
      heater_off();
      light_blink(3);
      use_pomp = desired_temp;
    } else if(temp <= desired_temp-2 && heater_state == 0){
      heater_on();
      light_blink(6);
      if (temp <= desired_temp-15){
        use_pomp = 5;
      }
    }

    if(LEFT_SW){
      desired_temp--;
      dot = prepare_digits(desired_temp, digits, 4);
      _delay_ms(150);
      was_click = 1;
    } else if(RIGHT_SW){
      desired_temp++;
      dot = prepare_digits(desired_temp, digits, 4);
      _delay_ms(150);
      was_click = 1;
    } else {
      // Display a bit longer current desired_temp.
      if (was_click == 1){
        _delay_ms(2500);   
        was_click = 0;
      }
      dot = prepare_digits(temp, digits, 4);
      _delay_ms(500);
    }
  }
  
}
